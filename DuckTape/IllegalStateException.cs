using System.Collections.Generic;

namespace DuckTape
{
	public class IllegalStateException : System.Exception
	{
		public IllegalStateException(string message) : base(message)
		{
		}
	}

}
