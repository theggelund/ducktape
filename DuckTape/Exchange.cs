using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace DuckTape
{

    public class Exchange : IExchange {

		private IExchangeMessage OutField;

        public const string RouteStop = "RouteStop";

        public const string Log = "Log";

        public string Id { get; private set; }

        public IDictionary<string, object> Properties { get; private set; }

        public IRouteContext RouteContext { get; private set; }

		public IContainerContext ContainerContext { get; private set; } 

		public IExchangeMessage In { get; set; }

		public IExchangeMessage Out {
			get { return OutField ?? (OutField = new DefaultMessage()); }
			set { OutField = value; }
		}

        public bool HasOut()
        {
            return OutField != null;
        }

        public Exchange(IRouteContext routeContext, IContainerContext containerContext, IExchangeMessage @in)
		{
            RouteContext = routeContext;
			ContainerContext = containerContext;
			In = @in;

            Properties = new Dictionary<string, object>();
            Properties[Log] = new StringBuilder();
		}
		
	}

}
