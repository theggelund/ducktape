using System.Collections.Generic;
using DuckTape.Definitions;

namespace DuckTape
{
	public class RouteDefinition {
		private readonly List<IRouteStepDefinition> Steps = new List<IRouteStepDefinition>();

		private bool Started;

//		public string Id { get; private set; }

		public IErrorHandler ErrorHandler { get; private set; }

		public IContainerContext ContainerContext { get; private set;}

		//public IConsumer Input { get; private set; }

		public RouteDefinition() {
			//ContainerContext = context;
			//Id = string.Format("{0}.{1}", ContainerContext.Id, ContainerContext.Routes.Count + 1);
		}

//		public void SetRouteId(string id) {
//			foreach(var route in ContainerContext.Routes) 
//			{
//				if (route != this && route.Id == id) 
//				{
//					throw new IllegalStateException("A route with same id already exists");
//				}
//			}
//
//			Id = id;
//		}

		public void Start() {
			Started = true;
		}

		public int Count() {
			return Steps.Count;
		}

		public IRouteStepDefinition Get(int index) {
			return Steps[index];
		}

		public void Add(IRouteStepDefinition nextStep) {
			if(Started) {
				throw new RouteAlreadyStartedException();
			}

			Steps.Add(nextStep);
		}
	}
}
