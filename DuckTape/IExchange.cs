using System.Collections.Generic;

namespace DuckTape
{
	public interface IExchange {

		IContainerContext ContainerContext { get; }

		IRouteContext RouteContext { get; }

        IDictionary<string, object> Properties { get; }

        string Id { get; }

		IExchangeMessage In { get; set;  }

		IExchangeMessage Out { get; set; }

        bool HasOut();
	}
}
