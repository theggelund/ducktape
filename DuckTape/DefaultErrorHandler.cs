﻿using System;

namespace DuckTape
{
	public class DefaultErrorHandler : IErrorHandler
	{
		public void OnException(Exception exception, RouteDefinition route, IExchange exchange)
		{
			Console.WriteLine(exception);
		}
	}
}

