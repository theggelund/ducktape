using System;
using System.Collections.Generic;

namespace DuckTape
{
	/// <summary>
	/// Service locator interface 
	/// </summary>
	public interface IServiceLocator {

		/// <summary>
		/// Get instance of T. 
		/// Must return null if no instance is available
		/// </summary>
		/// <typeparam name="T">Type of instance to get from service locator</typeparam>
		T Get<T>();

        /// <summary>
        /// Get instance of T. 
        /// Must return null if no instance is available
        /// </summary>
        /// <param name="name">Name of instance to get from service locator</param>
        /// <typeparam name="T">Type of instance to get from service locator</typeparam>
        T Get<T>(string name);
	}
	
}
