using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace DuckTape
{
	public class DefaultMessage : IExchangeMessage {
		public IDictionary<string, object> Headers { get; private set; }

		public object Body { get; set; }

		public DefaultMessage()
		{
			Headers = new Dictionary<string, object>();
		}

		public T BodyAs<T>() where T : class {
			return Body as T;
		}
	}




}
