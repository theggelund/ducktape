using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace DuckTape
{
	public interface IErrorHandler {
		void OnException(Exception exception, RouteDefinition route, IExchange exchange);
	}
	
}
