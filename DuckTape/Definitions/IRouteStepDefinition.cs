using System;

namespace DuckTape.Definitions
{
	public interface IRouteStepDefinition
	{
        IProcessor CreateProcessor(IContainerContext context);
	}
	
}
