﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace DuckTape.Components.Direct
{
    /// <summary>
    /// Convert message from component type to exchange
    /// </summary>
    public interface IConsumer {
        IEndpoint Endpoint { get; }
    }

    public class DirectConsumer : IConsumer
    {
        public IEndpoint Endpoint { get; private set; }

        public DirectConsumer(IEndpoint endpoint)
        {
            Endpoint = endpoint;
        }
    }

    public abstract class DefaultEndpoint : IEndpoint 
    {
        protected readonly IContainerContext Context;

        public IProcessor Processor { get; set; }

        public IList<IConsumer> Consumers { get; private set; }

        public abstract IConsumer CreateConsumer();

        public abstract IProducer CreateProducer();

        public virtual void Start() 
        {
            foreach(var service in GetServices()) 
            {
                service.Start();
            }
        }

        public void Stop() 
        {
            foreach(var service in GetServices()) {
                service.Stop();
            }
        }

        protected virtual IEnumerable<IServiceLifecycle> GetServices() 
        {
            var collection = new Collection<IServiceLifecycle>();
            if (Processor is IServiceLifecycle) {
                collection.Add(Processor as IServiceLifecycle);
            }

            foreach(var consumer in Consumers) {
                if (consumer is IServiceLifecycle) {
                    collection.Add(consumer as IServiceLifecycle);
                }
            }

            return collection;
        }

        protected DefaultEndpoint(IContainerContext context) 
        {
            Context = context;
            Consumers = new List<IConsumer>();
        }
    }

    public class DirectEndpoint : DefaultEndpoint
    {
        public DirectEndpoint(IContainerContext context) : base(context) 
        {
        }

        public override IProducer CreateProducer()
        {
            throw new NotImplementedException();
        }

        public override IConsumer CreateConsumer() {
            var consumer = new DirectConsumer(this);
            Consumers.Add(consumer);
            return consumer;
        }
    }

    public interface IServiceLifecycle 
    {
        void Start();

        void Stop();
    }

    public interface IEndpoint : IServiceLifecycle
    {
        IProcessor Processor { get; set; }

        IList<IConsumer> Consumers { get; }

        IConsumer CreateConsumer();

        IProducer CreateProducer();
    }
}

