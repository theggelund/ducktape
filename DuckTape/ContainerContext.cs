using System;
using System.Collections.Generic;

namespace DuckTape
{
	public class ContainerContext : IContainerContext {
        public bool Started { get; private set; }

        private string idField;

        public string Id
        {
            get { return idField; }
            set
            {
                if (Started)
                {
                    throw new IllegalStateException("Container is started");
                }
                idField = value;
            }
        }

		public IServiceLocator ServiceLocator { get; set; }

		public ITaskExecutor TaskExecutor { get; set; }

        public IList<IRouteContext> Routes { get; private set; }

		public IErrorHandler ErrorHandler { get; set; }

		public void AddRoutes(params RouteDefinition [] routes) 
        {

		}

        public void Start() 
        {
            if (string.IsNullOrEmpty(Id))
            {
                Id = Guid.NewGuid().ToString("D");
            }

            if (ErrorHandler == null)
            {
                ErrorHandler = new DefaultErrorHandler();
            }

            Started = true;
        }

        public void Stop() 
        {
            Started = false;
        }
	}
}
