﻿using System;

namespace DuckTape
{
    public interface ILogger
    {
        bool IsDebugEnabled { get; }

        void Debug(string message);

        bool IsInfoEnabled { get; }

        void Info(string message);
    }
}

