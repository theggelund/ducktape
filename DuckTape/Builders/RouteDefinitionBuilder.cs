using System;
using DuckTape.Processors;

namespace DuckTape.Builders
{
	public class RouteDefinitionBuilder {
		public RouteDefinition Route { get; private set; }

		public RouteDefinitionBuilder(string uri) {
			Route = new RouteDefinition();
		}
	}
}
