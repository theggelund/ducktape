using System;
using DuckTape.Processors;

namespace DuckTape.Builders
{

	public abstract class RouteBuilder 
	{
        protected RouteBuilder() {}

		//		protected RouteDefinitionBuilder From<T>() where T : IConsumer, new() 
//		{
//			// TODO
//			return From(new T());
//		}

//		protected RouteDefinitionBuilder From<T>(T consumer) where T : IConsumer 
//		{
//			return new RouteDefinitionBuilder(consumer);
//		}
//
		protected RouteDefinitionBuilder From(string uri) {
			return new RouteDefinitionBuilder(uri);
		}

		public abstract void Build();
	}
}
