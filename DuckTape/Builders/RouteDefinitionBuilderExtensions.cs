﻿using System;
using DuckTape.Processors;

namespace DuckTape.Builders
{
	public static class RouteDefinitionBuilderExtensions {
		public static RouteDefinitionBuilder Filter(this RouteDefinitionBuilder builder, Func<IExchange, bool> filterFunc) {
            builder.Route.Add(new FilterStepDefinition(filterFunc));
			return builder;
		}

        public static RouteDefinitionBuilder SetHeader(this RouteDefinitionBuilder builder, string headerName, object value) {
            builder.Route.Add(new HeaderEnricherStepDefinition(headerName, value));
            return builder;
        }

        public static RouteDefinitionBuilder To(this RouteDefinitionBuilder builder, string uri) {
            builder.Route.Add(new ToStepDefinition(uri));
            return builder;
        }
	}
}

