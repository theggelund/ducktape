using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace DuckTape
{

	/// <summary>
	/// Convert from exchange to component type
	/// </summary>
	public interface IProducer : IProcessor {
	}
	
}
