using System;

namespace DuckTape
{
	public interface IRouteHandler {
		RouteDefinition Route { get; }

		void SendExchange(IExchange exchange);
	}

	public class DefaultRouteHandler : IRouteHandler {
		public RouteDefinition Route { get; private set; }

		public void SendExchange(IExchange exchange)
		{
			for(int i = 0; i < Route.Count(); i++) 
			{
				PrepareExchange(exchange);


				var step = Route.Get(i);

//				try 
//				{
//					if (!step.Process(exchange)) {
//						return;
//					}
//				}
//				catch(Exception exception) 
//				{
//					HandleException(exception, exchange);
//					return;
//				}
			}
		}

		private void PrepareExchange(IExchange exchange)
		{
//			if(exchange.Out != null) {
//				exchange.In = exchange.Out;
//				exchange.Out = null;
//			}
		}

		private void HandleException(Exception exception, IExchange exchange)
		{
			if(Route.ErrorHandler != null) 
			{
				Route.ErrorHandler.OnException(exception, Route, exchange);
			}
			else if(Route.ContainerContext.ErrorHandler != null) 
			{
				Route.ContainerContext.ErrorHandler.OnException(exception, Route, exchange);
			}
		}
	}
	
}
