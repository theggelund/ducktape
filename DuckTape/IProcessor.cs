using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace DuckTape
{
	public interface IProcessor {
        /// <summary>
		/// Process the specified exchange.
		/// Return true if exchange should continue
		/// </summary>
		/// <param name="exchange">Exchange.</param>
        void Process(IExchange exchange);
	}
	
}
