using System;
using System.Collections.Generic;

namespace DuckTape
{
	public interface IContainerContext {
		string Id { get; }

		IErrorHandler ErrorHandler { get; set; }

		IServiceLocator ServiceLocator { get; }

		ITaskExecutor TaskExecutor { get; }

        IList<IRouteContext> Routes { get; }
	}
	
}
