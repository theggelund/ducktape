﻿using System;

namespace DuckTape.Processors
{
    public class GenericActionProcessor : IProcessor
    {
        private readonly Action<IExchange> Action;

        public IProcessor NextProcessor { get; private set; }

        public GenericActionProcessor(Action<IExchange> action)
        {
            Action = action;
        }

        public void Process(IExchange exchange)
        {
            Action.Invoke(exchange);
        }
    }
}

