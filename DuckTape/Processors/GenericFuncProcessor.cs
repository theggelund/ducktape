using System;

namespace DuckTape.Processors
{
    public class GenericFuncProcessor : IProcessor {
        private readonly Func<IExchange, bool> Func;

        public GenericFuncProcessor(Func<IExchange, bool> filter)
		{
			Func = filter;
		}
			
		public void Process(IExchange exchange)
        {
            if (!Func.Invoke(exchange)) {
                exchange.Properties[Exchange.RouteStop] = true;
            }
        }
	}
	
}
