﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using DuckTape.Extensions;

namespace DuckTape.Processors
{
    public class PipelineProcessor : IProcessor
    {
        private readonly IList<IProcessor> Processors;

        public PipelineProcessor(IEnumerable<IProcessor> processors)
        {
            Processors = new List<IProcessor>(processors);
        }

        public void Process(IExchange exchange)
        {
            foreach (var processor in Processors) {
                PrepareExchange(exchange);

                processor.Process(exchange);

                if (exchange.Properties.Get<bool>(Exchange.RouteStop))
                {
                    IProcessor processor1 = processor;
                    exchange.Debug(() => string.Format("Exchange with id [{0}] stopped after [{1}]", exchange.Id, processor1));
                    break;
                }
            }
        }

        private void PrepareExchange(IExchange exchange)
        {
            if (exchange.HasOut())
            {
                exchange.In = exchange.Out;
                exchange.Out = null;
            }
        }

    }
}

