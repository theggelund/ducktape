﻿using System;
using DuckTape.Definitions;

namespace DuckTape.Processors
{
    public class ToStepDefinition : IRouteStepDefinition
    {
        private readonly string Uri;

        public ToStepDefinition(string uri) {
            Uri = uri;
        }

        public IProcessor CreateProcessor(IContainerContext context)
        {
            return new ToProcessor(Uri);
        }
    }
}

