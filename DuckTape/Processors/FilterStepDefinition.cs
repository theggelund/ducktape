﻿using System;
using DuckTape.Definitions;

namespace DuckTape.Processors
{
	public class FilterStepDefinition : IRouteStepDefinition
	{
		private readonly Func<IExchange, bool> FilterFunc;

		public FilterStepDefinition(Func<IExchange, bool> filterFunc) 
		{
			FilterFunc = filterFunc;
		}

        public IProcessor CreateProcessor(IContainerContext context)
		{
			return new GenericFuncProcessor(FilterFunc);
		}
	}
}

