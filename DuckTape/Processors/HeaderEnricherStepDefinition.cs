﻿using System;
using DuckTape.Definitions;

namespace DuckTape.Processors
{
    public class HeaderEnricherStepDefinition : IRouteStepDefinition
    {
        private readonly Action<IExchange> HeaderEnricherAction;

        public HeaderEnricherStepDefinition(string name, object value) 
        {
            HeaderEnricherAction = exchange => exchange.In.Headers[name] = value;
        }

        public IProcessor CreateProcessor(IContainerContext context)
        {
            return new GenericActionProcessor(HeaderEnricherAction);
        }
    }
}

