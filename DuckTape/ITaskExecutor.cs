using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DuckTape
{
	public interface ITaskExecutor
	{
		Task Execute(Action action);

		Task Execute(Action action, DateTime at);
	}

	
}
