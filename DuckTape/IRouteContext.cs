﻿using System;
using System.Collections.Generic;

namespace DuckTape
{
    public interface IRouteContext
    {
        RouteDefinition RouteDefinition { get; }

        IList<IProcessor> Processors { get; }
    }
}

