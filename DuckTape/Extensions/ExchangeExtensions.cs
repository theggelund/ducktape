﻿using System;

namespace DuckTape.Extensions
{
    public static class ExchangeExtensions
    {
        public static void Debug(this IExchange exchange, Func<string> log) {
            var logger = exchange.Properties.Get<ILogger>(Exchange.Log);

            if (logger.IsDebugEnabled) {
                logger.Debug(log.Invoke());
            }
        }
    }
}

