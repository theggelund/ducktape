using System;
using System.Collections.Generic;

namespace DuckTape.Extensions
{
	public static class DictionaryExtensions {
		public static T Get<T>(this IDictionary<string, object> headers, string name)  {
			object item;

            if(headers.TryGetValue(name, out item) && item.GetType() is T) {
                return (T)item;
			}

            return default(T);
		}
	}
	
}
