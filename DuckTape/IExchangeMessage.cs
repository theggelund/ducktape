using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace DuckTape
{
	public interface IExchangeMessage {
		IDictionary<string, object> Headers { get; }

		object Body { get; set; }
	
		T BodyAs<T>() where T : class;
	}
}
