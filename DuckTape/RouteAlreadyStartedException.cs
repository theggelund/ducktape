using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace DuckTape
{
	public class RouteAlreadyStartedException : Exception
	{
		public RouteAlreadyStartedException() : base("Operation is not allowed after route is started")
		{}
	}
}
