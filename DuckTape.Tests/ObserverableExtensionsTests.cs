﻿using DuckTape;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using NUnit.Framework.SyntaxHelpers;
using DuckTape.Builders;
using DuckTape.Extensions;

namespace DuckTape.Tests
{
//	public class Observ : IObservable<string> 
//	{
//		private readonly IList<IObserver<string>> Subjects = new List<IObserver<string>>();
//
//		public IDisposable Subscribe(IObserver<string> observer)
//		{
//			Subjects.Add(observer);
//			return new Unsubscribe<string>(Subjects, observer);
//		}
//
//		private class Unsubscribe<T> : IDisposable {
//			private readonly IList<IObserver<T>> Subjects;
//
//			private readonly IObserver<T> UnsubscribeItem;
//
//			public Unsubscribe(IList<IObserver<T>> subjects, IObserver<T> unsubscribeItem) {
//				Subjects = subjects;
//				UnsubscribeItem = unsubscribeItem;
//			}
//
//			public void Dispose()
//			{
//				Subjects.Remove(UnsubscribeItem);
//			}
//		}
//	}


    [TestFixture]
    public class RouteBuilderTests {
        public class MyRouteBuilder : RouteBuilder {
            public override void Build()
            {
                From("direct:something")
                    .Filter(x => x.In.Headers.Get<string>("Content-Type") == @"text\html")
                    .To("direct:somethingElse");
            }
        }
    }


	[TestFixture]
	public class InterfaceWorkInProgress {

//		private class MockConsumer : IConsumer
//		{
//			public IRouteHandler RouteHandler {
//				get {
//					throw new NotImplementedException();
//				}
//			}
//		}
//
//		[Test]
//		public void Test() {
//
//				
//			var uri = new Uri("activemq:queue:receive");
//
//			Assert.AreEqual("", uri.GetLeftPart(UriPartial.Path));
//
//		}

	}
}

