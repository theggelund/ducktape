using DuckTape;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using NUnit.Framework.SyntaxHelpers;
using DuckTape.Builders;

namespace DuckTape.Tests
{
    [TestFixture]
    public class ContainerContextTests
    {
        [Test]
        public void IdIsAssignedOnStartIfNoneIsAlreadySpecified() 
        {
            var container = new ContainerContext();
            container.Start();

            Assert.That(container.Id, Is.Not.Empty);
        }

        [Test]
        public void IdIsNotOverwrittenOnStartIfAlreadySpecified()
        {
            var expected = Guid.NewGuid().ToString();

            var container = new ContainerContext
            {
                Id = expected
            };
            container.Start();

            Assert.That(container.Id, Is.EqualTo(expected));
        }

        [Test]
        public void StartedStatusIsFalseBeforeCallingStart()
        {
            var container = new ContainerContext();
            Assert.That(container.Started, Is.False);
        }

        [Test]
        public void StartedStatusIsTrueAfterCallingStart()
        {
            var container = new ContainerContext();
            container.Start();
            Assert.That(container.Started, Is.True);
        }

        [Test]
        public void StartedStatusIsFalseAfterCallingStop() 
        {
            var container = new ContainerContext();
            container.Start();
            Assert.That(container.Started, Is.True);
            container.Stop();
            Assert.That(container.Started, Is.False);
        }
    }
	
}
